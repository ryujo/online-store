package lv.javaguru.java2.servlet;

import lv.javaguru.java2.database.DBException;
import lv.javaguru.java2.database.jdbc.ProductDAO;
import lv.javaguru.java2.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class ProductController implements MVCController {

    @Autowired
    private ProductDAO productDAO;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        long id = 0;
        if (request.getParameterMap().containsKey("id")) {
            id = Long.valueOf(request.getParameter("id"));
        } else {
            Response.redirect(response, "/");
        }

        Product product = null;
        try {
            product = productDAO.getById(id);
        } catch (DBException e) {
            Response.error(response, 404);
            return;
        }

        if (product == null) {
            Response.error(response, 404);
            return;
        }

        Map<String, Object> data = new HashMap<>();
        data.put("product", product);
        request.setAttribute("data", data);

        Response.render(request, response, "/productView.jsp");
    }
}
