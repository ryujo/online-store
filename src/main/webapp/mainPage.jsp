<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <ul>
    <c:forEach var="category" items="${requestScope.data['categories']}">
        <li>
            <a href="">${category.name}</a>
        </li>
    </c:forEach>
    </ul>
    <ul>
    <c:forEach var="product" items="${requestScope.data['products']}">
        <li>
            <a href="product?id=${product.id}">${product.name}</a>
            <%--<a href="<c:url value="/product?id=${product.id}" />">${product.name}</a>--%>
        </li>
    </c:forEach>
    </ul>
</body>
</html>
