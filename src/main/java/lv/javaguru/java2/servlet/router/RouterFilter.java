package lv.javaguru.java2.servlet.router;

import lv.javaguru.java2.configuration.Application;
import lv.javaguru.java2.servlet.MVCController;
import lv.javaguru.java2.servlet.MainPageController;
import lv.javaguru.java2.servlet.PathsController;
import lv.javaguru.java2.servlet.ProductController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RouterFilter implements Filter {

    private Router router;

    public void init(FilterConfig filterConfig) throws ServletException {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Application.class);

        MainPageController mainPageController = (MainPageController) applicationContext.getBean(MainPageController.class);
        ProductController productController = (ProductController) applicationContext.getBean(ProductController.class);
        PathsController pathsController = (PathsController) applicationContext.getBean(PathsController.class);

        router = applicationContext.getBean(Router.class);

        router.addRoute("/", mainPageController);
        router.addRoute("/product", productController);
        router.addRoute("/paths", pathsController);
    }

    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;

//        String httpMethod = req.getMethod();
        String contextURI = req.getServletPath();
        MVCController controller = router.route(contextURI);

        if (controller != null) {
            controller.execute(req, resp);
        } else {
            filterChain.doFilter(request,response);
        }
    }

    public void destroy() {

    }

}
