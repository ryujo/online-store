package lv.javaguru.java2.database.jdbc;

import com.mysql.jdbc.Statement;
import lv.javaguru.java2.database.DAOInterface;
import lv.javaguru.java2.database.DBException;
import lv.javaguru.java2.domain.Product;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@Repository
public class ProductDAO extends DAO implements DAOInterface<Product> {

    public static final String TABLE_NAME = "products";

    private final String CREATE = "INSERT INTO " + TABLE_NAME + " VALUES (DEFAULT, ?, ?, ?, ?);";
    private final String GET_ALL = "SELECT * FROM " + TABLE_NAME + ";";
    private final String GET_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE id = ?;";
    private final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id = ?;";
    private final String UPDATE = "UPDATE " + TABLE_NAME + " SET "
            + "name = ?, "
            + "description = ?, "
            + "price = ?, "
            + "category_id = ? "
            + "WHERE id = ?";

    @Override
    public long create(Product product) throws DBException {
        long id = 0;
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(CREATE, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setLong(3, product.getPrice());
            // if cat id is 0, there's no category, so set null
            if (product.getCategoryId() == 0) {
                statement.setNull(4, Types.INTEGER);
            } else {
                statement.setLong(4, product.getCategoryId());
            }

            statement.executeUpdate();

            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                id = result.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        product.setId(id);
        return id;
    }

    @Override
    public void update(Product product) throws DBException {
        if (product.getId() == 0) {
            throw new IllegalArgumentException("Product should have an id.");
        }

        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(UPDATE);
            statement.setLong(5, product.getId());  // because WHERE clause is last
            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setLong(3, product.getPrice());
            if (product.getCategoryId() == 0) {
                statement.setNull(4, Types.INTEGER);
            } else {
                statement.setLong(4, product.getCategoryId());
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public void delete(Product product) throws DBException {
        if (product.getId() == 0) {
            throw new IllegalArgumentException("Product should have an id.");
        }

        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(DELETE);
            statement.setLong(1, product.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public Product getById(long id) throws DBException {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_BY_ID);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return productFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return null;
    }

    @Override
    public List<Product> getAll() throws DBException {
        LinkedList<Product> allProducts = new LinkedList<>();

        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_ALL);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                allProducts.add(productFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }

        return allProducts;
    }

    private Product productFromResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        String description = resultSet.getString("description");
        long price = resultSet.getLong("price");
        long categoryId = resultSet.getLong("category_id"); // if was NULL, 0 is returned
        return new Product(id, name, description, price, categoryId);
    }
}
