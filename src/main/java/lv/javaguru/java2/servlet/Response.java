package lv.javaguru.java2.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class Response {

    private Response(){}

    public static void render(HttpServletRequest request, HttpServletResponse response, String pathToView) throws ServletException, IOException {
        request.getServletContext().getRequestDispatcher(pathToView).forward(request, response);
    }

    public static void redirect(HttpServletResponse response, String redirectTo) throws IOException {
        response.sendRedirect(redirectTo);
    }

    public static void error(HttpServletResponse response, int errorNumber) throws IOException {
        response.sendError(errorNumber);
    }
    public static void error(HttpServletResponse response, int errorNumber, String message) throws IOException {
        response.sendError(errorNumber, message);
    }

    public static void text(HttpServletResponse response, String text) throws IOException {
        response.setHeader("Content-Type", "text/plain");
        response.setHeader("success", "yes");
        PrintWriter writer = response.getWriter();
        writer.write(text);
        writer.close();
    }

    public static void json(HttpServletResponse response, Object object) throws IOException {
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = objectWriter.writeValueAsString(object);
        response.setHeader("Content-Type", "application/json");
        PrintWriter writer = response.getWriter();
        writer.write(json);
        writer.close();
    }
}
