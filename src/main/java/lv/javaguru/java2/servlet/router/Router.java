package lv.javaguru.java2.servlet.router;

import lv.javaguru.java2.servlet.MVCController;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.util.*;

@Component
public class Router {

    private final Map<String, MVCController> routes = new HashMap<>();

    public void addRoute(String path, MVCController controller) {
        if (path == null || path.isEmpty()) {
            throw new IllegalArgumentException("Path is empty.");
        }
        if (controller == null) {
            throw new IllegalArgumentException("Controller is null.");
        }

        routes.put(path, controller);
    }

    public MVCController route(String path) {
        if (path == null || !routes.containsKey(path)) {
            throw new IllegalArgumentException("No route for " + path);
        }

        return routes.get(path);
    }
}
