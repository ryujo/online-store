package lv.javaguru.java2.servlet;

import lv.javaguru.java2.database.DBException;
import lv.javaguru.java2.database.jdbc.CategoryDAO;
import lv.javaguru.java2.database.jdbc.ProductDAO;
import lv.javaguru.java2.domain.Category;
import lv.javaguru.java2.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MainPageController implements MVCController {

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> data = new HashMap<>();

        List<Product> products = null;
        try {
            products = productDAO.getAll();
        } catch (DBException e) {
            e.printStackTrace();
        }

        List<Category> categories = null;
        try {
            categories = categoryDAO.getAll();
        } catch (DBException e) {
            e.printStackTrace();
        }

        data.put("products", products);
        data.put("categories", categories);

        request.setAttribute("data", data);

        Response.render(request, response, "/mainPage.jsp");
    }
}
