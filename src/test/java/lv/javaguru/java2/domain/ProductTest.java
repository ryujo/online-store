package lv.javaguru.java2.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {
    @Test
    public void comparisonTest() {
        Product p1 = new Product("Snake oil");
        Product p2 = new Product("Snake oil");
        Product p3 = new Product("Snake oils");
        assertTrue(p1.equals(p2));
        assertTrue(p2.equals(p1));
        assertTrue(p1.equals(p1));
        assertFalse(p1.equals(p3));
    }

    @Test
    public void testDisplayPrice() {
        Product p = new Product("Milk");
        assertEquals("0.00", p.getDisplayPrice());

        p.setPrice(1254);
        assertEquals("12.54", p.getDisplayPrice());
    }
}