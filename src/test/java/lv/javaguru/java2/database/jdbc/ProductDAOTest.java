package lv.javaguru.java2.database.jdbc;

import lv.javaguru.java2.database.DBException;
import lv.javaguru.java2.domain.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class ProductDAOTest {
    private ProductDAO productDAO;

    @Before
    public void setUp() throws Exception {
        this.productDAO = new ProductDAO();
    }

    @Test
    public void insertAndGetById() throws DBException {
        Product product = new Product("Goat cheese");
        long insertedId = productDAO.create(product);
        Product productFromDB = productDAO.getById(insertedId);
        System.out.println(product.toString());
        System.out.println(productFromDB.toString());
        assertEquals(product, productFromDB);
    }

    @Test
    public void nonExistentIdReturnsNull() throws DBException {
        Product nonExistentProduct = productDAO.getById(0);
        assertNull(nonExistentProduct);
    }

    @Test
    public void delete() throws DBException {
        Product product = new Product("Milky Way");
        productDAO.create(product);
        productDAO.delete(product);
        Product deletedProduct = productDAO.getById(product.getId());
        assertNull(deletedProduct);
    }

    @Test
    public void update() throws DBException {
        Product product = new Product("Rabioli");
        long id = productDAO.create(product);
        product.setName("Ravioli");
        productDAO.update(product);
        assertEquals("Ravioli", productDAO.getById(id).getName());
    }

    @Test
    public void getAll() throws DBException, SQLException {
        DAO dao = new DAO();
        Connection connection = null;
        try {
            connection = dao.getConnection();
            connection.createStatement().execute("delete from products;");
        } catch (DBException e) {
            e.printStackTrace();
        } finally {
            dao.closeConnection(connection);
        }

        productDAO.create(new Product("Milk, 1%"));
        productDAO.create(new Product("Milk, 2%"));
        productDAO.create(new Product("Milk, 3%"));

        List<Product> products = productDAO.getAll();
        for (Product p: products) {
            System.out.println(p.toString());
        }
        assertEquals(3, products.size());
    }

    @After
    public void tearDown() throws Exception {

    }

}