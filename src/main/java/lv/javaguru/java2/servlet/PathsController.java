package lv.javaguru.java2.servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class PathsController implements MVCController {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String responseText =
                "Servlet:\t" + request.getServletPath() + "\n"
                + "Context:\t" + request.getContextPath() + "\n"
                + "Path info:\t" + request.getPathInfo() + "\n";
        Response.text(response, responseText);
    }
}
