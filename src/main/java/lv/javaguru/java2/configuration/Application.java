package lv.javaguru.java2.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("lv.javaguru.java2")
public class Application {}
