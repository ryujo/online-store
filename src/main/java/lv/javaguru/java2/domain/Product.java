package lv.javaguru.java2.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Product {

    private long id;
    private String name;
    private String description;
    private long price;
    private long categoryId;

    public Product() {
        this("Crocodile meat");
    }

    public Product(String name) {
        this(0, name, "", 0, 0);
    }

    public Product(long id, String name, String description, long price, long categoryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        setPrice(price);
        this.categoryId = categoryId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPrice(long price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
        this.price = price;
    }

    public long getPrice() {
        return price;
    }

    public String getDisplayPrice() {
        double decimalPrice = price * 0.01;
        return String.format("%.2f", decimalPrice);
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if ((object == null) || !(object instanceof Product)) {
            return false;
        }

        Product that = (Product) object;
        return new EqualsBuilder()
                .append(id, that.getId())
                .append(name, that.getName())
                .append(description, that.getDescription())
                .append(price, that.getPrice())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(id)
                .append(name)
                .append(description)
                .append(price)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Product {"
                + "id: " + id + ", "
                + "name: " + name + ", "
                + "description: " + description + ", "
                + "price: " + getDisplayPrice() + ", "
                + "category: " + categoryId + "}";
    }
}
