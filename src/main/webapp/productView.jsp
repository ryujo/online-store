<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 8/15/16
  Time: 1:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <table>
        <tr>
            <td>Name:</td>
            <td>${requestScope.data['product'].name}</td>
        </tr>
        <tr>
            <td>Description:</td>
            <td>${requestScope.data['product'].description}</td>
        </tr>
        <tr>
            <td>Price:</td>
            <td>${requestScope.data['product'].displayPrice}</td>
        </tr>
    </table>
</body>
</html>
