package lv.javaguru.java2.servlet;

import lv.javaguru.java2.servlet.router.Router;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.*;

public class RouterTest {
    private Router router = new Router(true);

    private class ControllerWithoutConstructors implements MVCController {
        private ControllerWithoutConstructors() {}
        @Override
        public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        }
    }

    @Test
    public void canGetByClass() throws IllegalAccessException, InstantiationException {
        router.addRoute("/paths", PathsController.class);
        assertEquals("/paths", router.linkTo(PathsController.class));
    }

    @Test
    public void canGetByPath() throws IllegalAccessException, InstantiationException {
        router.addRoute("/paths", PathsController.class);
        assertTrue(router.controllerFor("/paths") instanceof PathsController);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFailsIfCantInstantiateController() throws IllegalAccessException, InstantiationException {
        router.addRoute("/fail", ControllerWithoutConstructors.class);
    }
}