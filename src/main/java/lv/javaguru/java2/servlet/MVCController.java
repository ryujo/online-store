package lv.javaguru.java2.servlet;

import lv.javaguru.java2.database.DBException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface MVCController {

    void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
